# Install the dependencies of the application.
FROM node:14 AS dependencies
WORKDIR /app
ADD package*.json /app/
RUN npm ci --production

# Copy the dependencies and the source code into a distroless image.
# For more information, please refer to https://github.com/GoogleContainerTools/distroless.
FROM gcr.io/distroless/nodejs:14
COPY --from=dependencies /app /app
ADD src/ /app/src/
WORKDIR /app
EXPOSE 5000
CMD [ "src/index.js" ]
