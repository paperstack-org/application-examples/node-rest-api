import express from 'express';
import { STATUS_CODES } from 'http';
import health from './routers/health.js';

const app = express();

app.use('/health', health);
app.use((req, res) => {
  const code = 404;

  return res.status(code).json({
    error: {
      reason: 'Unsupported Endpoint',
      title: STATUS_CODES[code],
      status: code,
    },
  });
});

export default app;
