import os from 'os';
import { Router } from 'express';

const timestamp = new Date().toISOString();
const startedAt = `${timestamp.substring(0, timestamp.length - 5)}Z`;
const hostname = os.hostname();
const router = Router();

router.get('/', (req, res) => {
  return res.status(200).json({
    data: {
      hostname,
      started_at: startedAt,
      version: 'dev',
    },
  });
});

export default router;
