import app from './app.js';

app.listen(5000, () => {
  process.stdout.write('Service online: http://0.0.0.0:5000\n');
});
