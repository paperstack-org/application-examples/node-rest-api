# Node REST API :zap:

An example of a REST API with [Node](https://nodejs.org/en/). It uses [Express](https://expressjs.com/) as the web framework.

[![pipeline status](https://gitlab.com/paperstack-org/application-examples/node-rest-api/badges/main/pipeline.svg)](https://gitlab.com/paperstack-org/application-examples/node-rest-api/-/commits/main)

## Development 🔧

Start the development container and run `npm run dev`. The application will automatically restart on code changes and you can visit it at [localhost:5000](http://localhost:5000/health).

## Production 🚀

To run the application in production execute `npm start`.

## License 📄

This project is licensed under the terms of the [MIT license](./LICENSE.md).
